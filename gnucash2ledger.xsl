<!-- -*- mode:xml -*-
// ================================================================================
// GitLab:    http://gitlab.com/voidyourwarranty/gnucash2ledger
// File:      gnucash2ledger.xsl
// Date:      2018-08-10
// Author:    voidyourwarranty
// Purpose:   Migrate a GnuCash Database to Ledger CLI
// ================================================================================
//-->

<!--
The name spaces declared here need to use precisely the same URIs as the corresponding
ones in the GnuCash XML data base.
//-->

<xsl:stylesheet version="1.0"
  xmlns:gnc="http://www.gnucash.org/XML/gnc"
  xmlns:act="http://www.gnucash.org/XML/act"
  xmlns:cmdty="http://www.gnucash.org/XML/cmdty"
  xmlns:trn="http://www.gnucash.org/XML/trn"
  xmlns:split="http://www.gnucash.org/XML/split"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xdt="http://www.w3.org/2005/02/xpath-datatypes">
  
<xsl:output method="text"/>

<!--
This matches the main tag of the XML data base.
//-->

<xsl:template match="/gnc-v2">

  <!--
  First, produce the list of all commodities.
  //-->
  
  <xsl:text>; --------------------------------------------------------------------------------&#10;</xsl:text>
  <xsl:text>; Commodities&#10;</xsl:text>
  <xsl:text>; --------------------------------------------------------------------------------&#10;</xsl:text>
  <xsl:for-each select="gnc:book/gnc:commodity">
    <xsl:sort order="ascending" select="cmdty:name"/>
    <xsl:text>commodity "</xsl:text><xsl:value-of select="cmdty:id" /><xsl:text>"</xsl:text>
    <xsl:if test="boolean(cmdty:name)"><xsl:text>&#10;  note </xsl:text><xsl:value-of select="cmdty:name" />
      <xsl:if test="boolean(cmdty:xcode)"><xsl:text> (</xsl:text><xsl:value-of select="cmdty:xcode" /><xsl:text>)</xsl:text></xsl:if></xsl:if>
    <xsl:text>&#10;  format 1,000,000.00 "</xsl:text><xsl:value-of select="cmdty:id" /><xsl:text>"</xsl:text>
    <xsl:text>&#10;  nomarket&#10;  default</xsl:text>
    <xsl:text>&#10;&#10;</xsl:text>
  </xsl:for-each>

  <!--
  Then produce the account hierarchy.
  //-->
  
  <xsl:for-each select="gnc:book/gnc:account[act:type='ROOT']">
    <!-- <xsl:text>FOUND ROOT</xsl:text><xsl:text>&#10;</xsl:text> //-->
    <xsl:text>; --------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:text>; Account Hierarchy&#10;</xsl:text>
    <xsl:text>; --------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:variable name="anchor"><xsl:value-of select="act:id" /></xsl:variable>
    <xsl:apply-templates select="/gnc-v2/gnc:book/gnc:account[act:parent=$anchor]" />
  </xsl:for-each>

  <!--
  Finally, output all transactions.
  //-->
  
  <xsl:text>; --------------------------------------------------------------------------------&#10;</xsl:text>
  <xsl:text>; Transactions&#10;</xsl:text>
  <xsl:text>; --------------------------------------------------------------------------------&#10;</xsl:text>

  <xsl:for-each select="gnc:book/gnc:transaction">
    <xsl:sort order="ascending" select="substring (normalize-space(trn:date-posted),1,10)"/>
    <xsl:value-of select="substring (normalize-space(trn:date-posted),1,10)" />
    <xsl:text>  </xsl:text><xsl:value-of select="trn:description" /><xsl:text>&#10;</xsl:text>
    <xsl:variable name="currency"><xsl:value-of select="trn:currency/cmdty:id" /></xsl:variable>
    <xsl:for-each select="trn:splits/trn:split">
      <xsl:text>  </xsl:text>
      <xsl:variable name="account"><xsl:value-of select="split:account" /></xsl:variable>
      <xsl:for-each select="/gnc-v2/gnc:book/gnc:account[act:id=$account]"><xsl:call-template name="account-chain" /></xsl:for-each>
      <xsl:text>  (</xsl:text><xsl:value-of select="split:quantity" /><xsl:text>  </xsl:text>
      <xsl:text>"</xsl:text><xsl:value-of select="/gnc-v2/gnc:book/gnc:account[act:id=$account]/act:commodity/cmdty:id" /><xsl:text>"</xsl:text>
      <xsl:text>)</xsl:text>
      <xsl:if test="not($currency=/gnc-v2/gnc:book/gnc:account[act:id=$account]/act:commodity/cmdty:id)">
	<xsl:text> @ (</xsl:text><xsl:value-of select="split:value" /><xsl:text>  </xsl:text>
	<xsl:text>"</xsl:text><xsl:value-of select="$currency" /><xsl:text>" / (</xsl:text><xsl:value-of select="split:quantity" />
	<xsl:text>))</xsl:text>
      </xsl:if>
      <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
  </xsl:for-each>
</xsl:template>

<!--
The following template matches an account and loops over all accounts
that have the given account as their parent.
//-->

<xsl:template match="/gnc-v2/gnc:book/gnc:account">
  <xsl:variable name="anchor"><xsl:value-of select="act:id" /></xsl:variable>
  <xsl:if test="not(boolean(//gnc:book/gnc:account[act:parent=$anchor]))">
    <xsl:text>account </xsl:text>
    <xsl:variable name="parent"><xsl:value-of select="act:parent" /></xsl:variable>
    <xsl:for-each select="/gnc-v2/gnc:book/gnc:account[act:id=$parent and act:type!='ROOT']"><xsl:call-template name="account-chain" /></xsl:for-each>
    <xsl:value-of select="act:name" /><xsl:text>&#10;&#10;</xsl:text>
  </xsl:if>
  <xsl:apply-templates select="/gnc-v2/gnc:book/gnc:account[act:parent=$anchor]"><xsl:sort select="act:name" /></xsl:apply-templates>
</xsl:template>

<!--
The following template matches an account. It prints the full account path including all its non-root parents,
//-->

<xsl:template name="account-chain">
  <xsl:variable name="parent"><xsl:value-of select="act:parent" /></xsl:variable>
  <xsl:for-each select="/gnc-v2/gnc:book/gnc:account[act:id=$parent and act:type!='ROOT']"><xsl:call-template name="account-chain" /></xsl:for-each>
  <xsl:value-of select="act:name" /><xsl:text>:</xsl:text>
</xsl:template>

</xsl:stylesheet>

<!--
// ================================================================================
// End of file.
// ================================================================================
//-->
