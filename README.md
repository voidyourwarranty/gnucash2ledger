# gnucash2ledger

This project is an XSL 1.0 transformation `gnucash2ledger.xsl` in order to migrate from
[GnuCash](https://www.gnucash.org/) to the command line accounting tool [ledger-cli](http://ledger-cli.org/index.html).

It was tested it with GnuCash, version 2.6.12, and with Ledger, version 3.1.1-20160111. Since I have already migrated my
personal data base, I do not plan to maintain this project any further and therefore place it in the public domain.

## Setup

GnuCash stores its database in a single `gzip` compressed XML file. We assume that this file has already been
decompressed and is made available as `gnucash-database.xml`. In my case, this file begins with
```
<?xml version="1.0" encoding="utf-8" ?>
<gnc-v2
     xmlns:gnc="http://www.gnucash.org/XML/gnc"
     xmlns:act="http://www.gnucash.org/XML/act"
```

The migration is done by applying an XSL 1.0 transformation to that XML file in order to produce a single ledger text
file. The XSL transformation is encoded in `gnucash2ledger.xsl`. We refer to the output ledger file as `output.lgr`.

## XSL Transformations

The XSL transformation can be performed by various tools. The following examples show the typical command line and also
indicate how to install the prerequisites on a typical Ubuntu system (my system is a Kubuntu 16.04.1 LTS).

### Xalan

```
sudo apt-get install xalan
xalan -xsl gnucash2ledger.xsl -in gnucash-database.xml >output.lgr
```

### Saxon

```
sudo apt-get install libsaxon-java default-jre
java -jar /usr/share/java/saxon.jar gnucash-database.xml gnucash2ledger.xsl >output.lgr 
```

### Xsltproc

```
sudo apt-get install xsltproc 
xsltproc gnucash2ledger.xsl gnucash-database.xml >output.lgr
```

All three outputs agree except that Saxon uses a different order than the other two when items are sorted
alphabetically.

Saxon appears to be the fastest, Xsltproc the slowest of the XSL transformations. Also note that Xsltproc is limited to
XSLT 1.0 as opposed to 2.0 (which is sufficient for our purposes though).

## Limitations and Technical Details

The present XSL transformation generally migrates commodities, accounts, transactions and their splits, but no more. Due
to the different concepts underlying GnuCash and Ledger, a number of discrepancies remain. The following GnuCash data are
not exported:
* commodity prices (some can be inferred from transactions that contain lines [GnuCash term: _split_] in accounts of
  different commodity).
* online sources of commodity prices
* account types (asset, liability, stock, etc.)
* the commodities associated with the individual accounts (Ledger accounts can contain transactions in any commodity)
* transaction date entered
* reconciled state of splits
* any special configured reports, invoices, etc.

When exporting transactions, for all _splits_, we record the transaction with its _quantity_ (GnuCash term) and in the
commodity that GnuCash associates with the respective account. GnuCash also stores a commodity with each transaction and
then records a _value_ of each split.

Whenever for a given split, the transaction commodity differs from the account commodity that we export, then we also
add ledger's `@` notation in order to record the _value_ of the split in transaction commodity, normalized per unit
_quantity_.

This way, ledger sees only balanced transactions, and whenever a line of a transaction is in a different commodity, we
also record the unit price of that commodity in the commodity that GnuCash associates with the transaction.

The following probably needs to be adjusted manually:
* Number formatting for the various commodities if it differs from the default `1,000,000.00 XYZ`.
* Transactions in GnuCash may have splits of quantity and value zero, but in different commodities e.g. `(0/100 EUR)` and
  `(0/100 USD)`. These splits make no sense, but apparently such splits remain after extensive editing by some accident. Out
  XSLT transformation adds these lines and also tries to compute the unit cost of the commodity,
  resulting in expressions such as `((0/100 EUR) / (0/100))` which give a division by zero error in Ledger. Such lines
  with zero contribution therefore need to be removed manually.
  (It would certainly be possible to suppress zero lines in the XSL transformation, but for my purposes this would have
  been a waste of time).
* I also had to fix a few transactions that did not actually balance in GnuCash due to data entry errors that GnuCash
  did not enfore correction of.

I recommend to carefully inspect the resultiung ledger file and manually verify in a number of instances whether all
features have been converted satisfactorily. This project is just what worked in my case and, of course, I cannot
guarantee that _your_ database is exported correctly. This project comes with no warranty whatsoever!
